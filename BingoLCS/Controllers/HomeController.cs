﻿using BingoLCS.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BingoLCS.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var itensDoBingo = new Dictionary<int, string>();

            #region Itens
            itensDoBingo.Add(1, "Mystic Mine Ganhando Jogo");
            itensDoBingo.Add(2, "Cheater / Roubo");
            itensDoBingo.Add(3, "Foice + Enforcer + Gryphon");
            itensDoBingo.Add(4, "Paulo PRRJ Topando ");
            itensDoBingo.Add(5, "Paulo PRRJ Jogando na Stream");
            itensDoBingo.Add(6, "Deck Meme Ganhando de Meta");
            itensDoBingo.Add(7, "Flunder Ganhando na Base do Shifter");
            itensDoBingo.Add(8, "50% do Top Tendo Mistura de Brave Token");
            itensDoBingo.Add(9, "Deck Ruim Topando");
            itensDoBingo.Add(10, "Pak Topando");
            itensDoBingo.Add(11, "Pro-Player Perdendo Pra Random");
            itensDoBingo.Add(12, "Judeu Sendo Burro");
            itensDoBingo.Add(13, "Rule Shark");
            itensDoBingo.Add(14, "Prank-Kids Ganhando no Tempo");
            itensDoBingo.Add(15, "Slowplay Para Ganhar no Tempo");
            itensDoBingo.Add(16, "Tech Questionavel");
            itensDoBingo.Add(17, "Tech INSANA");
            itensDoBingo.Add(18, "Abrir Sem Hand Trap e Desistir Turno 2");
            itensDoBingo.Add(19, "Mão COMPLETAMENTE ABSURDA!");
            itensDoBingo.Add(20, "Mão com 4 Garnet");
            itensDoBingo.Add(21, "Gameloss");
            itensDoBingo.Add(22, "Skill Drain :)");
            itensDoBingo.Add(23, "Aleister Tomando Interação mas tendo Ivocation na Mão");
            itensDoBingo.Add(24, "Token do Nibiru Turbo");
            itensDoBingo.Add(25, "Jogar pra cima de 3 ou mais Hand Traps");
            itensDoBingo.Add(26, "Ganhar por Causa de Carta Limitada");
            itensDoBingo.Add(27, "Perder pra 1 Hand Trap");
            itensDoBingo.Add(28, "Missplay FUDIDA");
            itensDoBingo.Add(29, "DESTINY DRAW ");
            itensDoBingo.Add(30, "Magicians Souls Comprando Garnet");
            itensDoBingo.Add(31, "Zeus de 4 de Ladinho ( ͡° ͜ʖ ͡°)");
            itensDoBingo.Add(32, "Mirror de Flunder KEKW");
            itensDoBingo.Add(33, "7 ou mais Negates");
            itensDoBingo.Add(34, "80% do Top Tendo Mistura de DPE, Brave, Souls e Scythe");
            #endregion

            var random = new Random();

            var itensAleatorio = itensDoBingo.OrderBy(x => random.Next(0,32)).Distinct().ToList();

            while (itensAleatorio.Count() != 24)
            {
                itensAleatorio.Remove(itensAleatorio.First());
            }

            var cartelaDoBingo = itensAleatorio.ToDictionary(x => x.Key, x => x.Value);

            var bingoViewModel = new BingoViewModel { CartelaDoBingo = cartelaDoBingo };

            return View(bingoViewModel);
        }
    }
}